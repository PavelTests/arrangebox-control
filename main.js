var ArrangeBox = /** @class */ (function () {
    function ArrangeBox() {
        var _this = this;
        this.lengthInitialValue = 5;
        this.selectedAvailable = [];
        this.selectedTarget = [];
        this.container = document.querySelector('.container');
        var initialValueAvailable = [];
        var initialValueTarget = [];
        var savedTarget = localStorage.getItem('target');
        //get values
        if (savedTarget) {
            initialValueTarget = JSON.parse(savedTarget);
        }
        for (var i = 0; i < this.lengthInitialValue; i++) {
            initialValueAvailable.push(this.generateRandomString(5));
            if (!savedTarget) {
                initialValueTarget.push(this.generateRandomString(5));
            }
        }
        //creating element
        this.liveUListSource = this.createList(initialValueAvailable);
        this.liveUListTarget = this.createList(initialValueTarget);
        this.initialListSource = Array.from(this.liveUListSource.children);
        this.initialListTarget = Array.from(this.liveUListTarget.children);
        var containerSource = this.createItemsContainer(this.liveUListSource, "Available");
        var containerTarget = this.createItemsContainer(this.liveUListTarget, "Selected");
        var transferButtons = this.createButtons(['>', '<', '>>', '<<'], [
            function (e) { return _this.handleTransferTarget(e); },
            function (e) { return _this.handleTransferSource(e); },
            function (e) { return _this.handleTransferAllTarget(e); },
            function (e) { return _this.handleTransferAllSource(e); }
        ], 4);
        var sortButtonsSource = this.createButtons(['\u2191', "\u2B71", "\u2193", "\u2913"], [
            function (e) { return _this.handleUp(e, _this.liveUListSource, _this.selectedAvailable); },
            function (e) { return _this.handleAllUp(e, _this.liveUListSource, _this.selectedAvailable); },
            function (e) { return _this.handleDown(e, _this.liveUListSource, _this.selectedAvailable); },
            function (e) { return _this.handleAllDown(e, _this.liveUListSource, _this.selectedAvailable); }
        ], 4);
        var sortButtonsTarget = this.createButtons(['\u2191', "\u2B71", "\u2193", "\u2913"], [
            function (e) { return _this.handleUp(e, _this.liveUListTarget, _this.selectedTarget); },
            function (e) { return _this.handleAllUp(e, _this.liveUListTarget, _this.selectedTarget); },
            function (e) { return _this.handleDown(e, _this.liveUListTarget, _this.selectedTarget); },
            function (e) { return _this.handleAllDown(e, _this.liveUListTarget, _this.selectedTarget); }
        ], 4);
        var controlsButtons = this.createButtons(['Get current selected', 'Reset to initial', 'Set selected (to localStr)', 'Remove selected (from localStr)', 'Create new'], [
            function (e) { return _this.getSelected(); },
            function (e) { return _this.setInitialValue(); },
            function (e) { return _this.saveTarget(); },
            function (e) { return _this.removeTarget(); },
            function (e) { return _this.createNewInstance(); },
        ], 5);
        var arrangeBox = document.createElement('div');
        arrangeBox.classList.add('arrange-box');
        var data = document.createElement('div');
        data.classList.add('data');
        var control = document.createElement('div');
        control.classList.add('control');
        //add all to html
        control.append(controlsButtons);
        data.append(sortButtonsSource, containerSource, transferButtons, containerTarget, sortButtonsTarget);
        arrangeBox.append(data, control);
        this.container.append(arrangeBox);
        this.setContainerListeners(containerSource, containerTarget);
    }
    ArrangeBox.prototype.generateRandomString = function (length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() *
                characters.length));
        }
        return result;
    };
    ArrangeBox.prototype.setContainerListeners = function (source, target) {
        var _this = this;
        source.addEventListener('click', function (e) { return _this.handleClickItem(e, 'selectedAvailable'); });
        source.addEventListener('dblclick', function (e) { return _this.handleDBClickItem(e, _this.selectedAvailable, _this.liveUListTarget); });
        target.addEventListener('click', function (e) { return _this.handleClickItem(e, 'selectedTarget'); });
        target.addEventListener('dblclick', function (e) { return _this.handleDBClickItem(e, _this.selectedTarget, _this.liveUListSource); });
    };
    ArrangeBox.prototype.createList = function (values) {
        var ul = document.createElement('ul');
        ul.className = 'list';
        for (var i = 0; i < values.length; i++) {
            var li = document.createElement('li');
            li.className = 'item';
            li.innerText = values[i];
            ul.append(li);
        }
        return ul;
    };
    ArrangeBox.prototype.createItemsContainer = function (list, headerTitle) {
        var _this = this;
        var container = document.createElement('div');
        container.classList.add('items-container');
        var header = document.createElement('header');
        header.innerText = headerTitle;
        var divFilter = document.createElement('div');
        divFilter.className = 'filter';
        var input = document.createElement('input');
        input.setAttribute("placeholder", "Search");
        input.setAttribute("type", "text");
        if (headerTitle === "Available") {
            input.addEventListener('input', function (e) { return _this.handleFilter(e, _this.liveUListSource); });
        }
        else {
            input.addEventListener('input', function (e) { return _this.handleFilter(e, _this.liveUListTarget); });
        }
        divFilter.append(input);
        container.append(header, divFilter, list);
        return container;
    };
    ArrangeBox.prototype.createButtons = function (content, actions, amount) {
        var container = document.createElement('div');
        container.className = "buttons-container";
        for (var i = 0; i < amount; i++) {
            var button = document.createElement('button');
            button.innerText = content[i];
            button.addEventListener('click', actions[i]);
            container.append(button);
        }
        return container;
    };
    ArrangeBox.prototype.clearSelected = function (selected) {
        selected.forEach(function (li) { return li.classList.remove('active'); });
        while (selected.length) {
            selected.pop();
        }
    };
    //HANDLERS //this - ArrangeBox
    //CLICK ITEMS
    ArrangeBox.prototype.handleClickItem = function (event, selected) {
        var item = event === null || event === void 0 ? void 0 : event.target;
        if (item && item.classList.contains('item')) {
            if (item.classList.contains("active")) {
                this[selected] = this[selected].filter(function (li) { return li !== item; });
                item.classList.remove('active');
            }
            else {
                this[selected].push(item);
                item.classList.add('active');
            }
        }
    };
    ArrangeBox.prototype.handleDBClickItem = function (event, selected, to) {
        var item = event === null || event === void 0 ? void 0 : event.target;
        if (item && item.classList.contains('item')) {
            var li = event.target;
            to.append(li);
            this.clearSelected(selected);
        }
    };
    //TRANSFER
    ArrangeBox.prototype.handleTransferTarget = function (e) {
        var _a;
        (_a = this.liveUListTarget).append.apply(_a, this.selectedAvailable);
        this.clearSelected(this.selectedAvailable);
    };
    ArrangeBox.prototype.handleTransferSource = function (e) {
        var _a;
        (_a = this.liveUListSource).append.apply(_a, this.selectedTarget);
        this.clearSelected(this.selectedTarget);
    };
    ArrangeBox.prototype.handleTransferAllTarget = function (e) {
        var _a;
        (_a = this.liveUListTarget).append.apply(_a, this.liveUListSource.children);
        this.clearSelected(this.selectedAvailable);
    };
    ArrangeBox.prototype.handleTransferAllSource = function (e) {
        var _a;
        (_a = this.liveUListSource).append.apply(_a, this.liveUListTarget.children);
        this.clearSelected(this.selectedTarget);
    };
    //FILTER
    ArrangeBox.prototype.handleFilter = function (e, list) {
        var _a, _b;
        var input = e.target;
        var value = input.value.trim().toLocaleLowerCase();
        for (var i = 0; i < list.children.length; i++) {
            var li = list.children[i];
            if ((_b = (_a = li.textContent) === null || _a === void 0 ? void 0 : _a.toLocaleLowerCase()) === null || _b === void 0 ? void 0 : _b.includes(value)) {
                li.style.display = "block";
            }
            else {
                li.style.display = "none";
            }
        }
    };
    //SORT
    ArrangeBox.prototype.handleAllDown = function (e, list, selected) {
        list.append.apply(list, selected);
        // this.clearSelected(selected);
    };
    ArrangeBox.prototype.handleAllUp = function (e, list, selected) {
        list.prepend.apply(list, selected);
        // this.clearSelected(selected);
    };
    ArrangeBox.prototype.handleDown = function (e, list, selected) {
        var _a;
        if (selected.length === 0)
            return;
        // O(n^2)
        var sortSelected = [];
        var _loop_1 = function (i) {
            var li = selected.find(function (li) { return li === list.children[i]; });
            if (li) {
                sortSelected.push(li);
            }
        };
        for (var i = 0; i < list.children.length; i++) {
            _loop_1(i);
        }
        var last = sortSelected.length - 1;
        if (sortSelected[last].nextElementSibling) {
            for (var i = last; i >= 0; i--) {
                var li = sortSelected[i];
                (_a = li.nextElementSibling) === null || _a === void 0 ? void 0 : _a.after(li);
            }
        }
        // this.clearSelected(selected);
    };
    ArrangeBox.prototype.handleUp = function (e, list, selected) {
        var _a, _b;
        if (selected.length === 0)
            return;
        // O(n^2)
        var sortSelected = [];
        var _loop_2 = function (i) {
            var li = selected.find(function (li) { return li === list.children[i]; });
            if (li) {
                sortSelected.push(li);
            }
        };
        for (var i = 0; i < list.children.length; i++) {
            _loop_2(i);
        }
        if ((_a = sortSelected[0]) === null || _a === void 0 ? void 0 : _a.previousElementSibling) {
            for (var i = 0; i < sortSelected.length; i++) {
                var li = sortSelected[i];
                (_b = li.previousElementSibling) === null || _b === void 0 ? void 0 : _b.before(li);
            }
        }
        // this.clearSelected(selected);
    };
    //CONTROLS
    ArrangeBox.prototype.setInitialValue = function () {
        var _a, _b;
        this.clearSelected(this.selectedAvailable);
        this.clearSelected(this.selectedTarget);
        this.liveUListTarget.textContent = '';
        this.liveUListSource.textContent = '';
        (_a = this.liveUListSource).append.apply(_a, this.initialListSource);
        (_b = this.liveUListTarget).append.apply(_b, this.initialListTarget);
    };
    ArrangeBox.prototype.setAvailableValue = function (values) {
        this.clearSelected(this.selectedAvailable);
        this.liveUListSource.textContent = '';
        for (var i = 0; i < values.length; i++) {
            var li = document.createElement('li');
            li.innerHTML = values[i];
            this.liveUListSource.append(li);
        }
    };
    ArrangeBox.prototype.getSelected = function () {
        var values = [];
        for (var i = 0; i < this.liveUListTarget.children.length; i++) {
            var li = this.liveUListTarget.children[i];
            values.push(li.textContent);
        }
        alert(JSON.stringify(values));
        console.log(JSON.stringify(values));
        return values;
    };
    ArrangeBox.prototype.saveTarget = function () {
        var target = Array
            .from(this.liveUListTarget.children)
            .map(function (el) { return el.textContent; });
        localStorage.setItem('target', JSON.stringify(target));
    };
    ArrangeBox.prototype.removeTarget = function () {
        localStorage.removeItem('target');
    };
    ArrangeBox.prototype.createNewInstance = function () {
        var newBox = new ArrangeBox();
    };
    return ArrangeBox;
}());
var arrangeBox = new ArrangeBox();
