class ArrangeBox {
    private lengthInitialValue = 5;
    private initialListSource: Element[];
    private initialListTarget: Element[];

    private selectedAvailable: HTMLLIElement[] = [];
    private selectedTarget: HTMLLIElement[] = [];

    private liveUListSource!: HTMLUListElement;
    private liveUListTarget!: HTMLUListElement;

    private container: HTMLDivElement;

    constructor() {
        this.container = document.querySelector('.container')!;

        const initialValueAvailable: string[] = [];
        let initialValueTarget: string[] = [];

        const savedTarget = localStorage.getItem('target');

        //get values
        if (savedTarget) {
            initialValueTarget = JSON.parse(savedTarget) as string[];
        }
        for (let i = 0; i < this.lengthInitialValue; i++) {
            initialValueAvailable.push(this.generateRandomString(5));
            if (!savedTarget) {
                initialValueTarget.push(this.generateRandomString(5));
            }
        }

        //creating element
        this.liveUListSource = this.createList(initialValueAvailable);
        this.liveUListTarget = this.createList(initialValueTarget);

        this.initialListSource = Array.from(this.liveUListSource.children);
        this.initialListTarget = Array.from(this.liveUListTarget.children);

        const containerSource = this.createItemsContainer(this.liveUListSource, "Available");
        const containerTarget = this.createItemsContainer(this.liveUListTarget, "Selected");

        const transferButtons = this.createButtons(
            ['>', '<', '>>', '<<'],
            [
                (e) => this.handleTransferTarget(e),
                (e) => this.handleTransferSource(e),
                (e) => this.handleTransferAllTarget(e),
                (e) => this.handleTransferAllSource(e)
            ],
            4);
        const sortButtonsSource = this.createButtons(
            ['\u2191', `\u2B71`, `\u2193`, `\u2913`],
            [
                (e) => this.handleUp(e, this.liveUListSource, this.selectedAvailable),
                (e) => this.handleAllUp(e, this.liveUListSource, this.selectedAvailable),
                (e) => this.handleDown(e, this.liveUListSource, this.selectedAvailable),
                (e) => this.handleAllDown(e, this.liveUListSource, this.selectedAvailable)
            ],
            4
        )
        const sortButtonsTarget = this.createButtons(
            ['\u2191', `\u2B71`, `\u2193`, `\u2913`],
            [
                (e) => this.handleUp(e, this.liveUListTarget, this.selectedTarget),
                (e) => this.handleAllUp(e, this.liveUListTarget, this.selectedTarget),
                (e) => this.handleDown(e, this.liveUListTarget, this.selectedTarget),
                (e) => this.handleAllDown(e, this.liveUListTarget, this.selectedTarget)
            ],
            4
        )
        const controlsButtons = this.createButtons(
            ['Get current selected', 'Reset to initial', 'Set selected (to localStr)', 'Remove selected (from localStr)', 'Create new'],
            [
                (e) => this.getSelected(),
                (e) => this.setInitialValue(),
                (e) => this.saveTarget(),
                (e) => this.removeTarget(),
                (e) => this.createNewInstance(),
            ],
            5
        )

        const arrangeBox = document.createElement('div');
        arrangeBox.classList.add('arrange-box');
        const data = document.createElement('div');
        data.classList.add('data');
        const control = document.createElement('div');
        control.classList.add('control');

        //add all to html
        control.append(controlsButtons);
        data.append(sortButtonsSource, containerSource, transferButtons, containerTarget, sortButtonsTarget)
        arrangeBox.append(data, control);
        this.container.append(arrangeBox);


        this.setContainerListeners(containerSource, containerTarget);
    }

    generateRandomString(length: number) {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() *
                characters.length));
        }

        return result;
    }

    setContainerListeners(source: HTMLDivElement, target: HTMLDivElement) {
        source.addEventListener('click', (e) => this.handleClickItem(e, 'selectedAvailable'));
        source.addEventListener('dblclick', (e) => this.handleDBClickItem(e, this.selectedAvailable, this.liveUListTarget));
        target.addEventListener('click', (e) => this.handleClickItem(e, 'selectedTarget'));
        target.addEventListener('dblclick', (e) => this.handleDBClickItem(e, this.selectedTarget, this.liveUListSource));
    }

    createList(values: string[]) {
        const ul = document.createElement('ul');
        ul.className = 'list';
        for (let i = 0; i < values.length; i++) {
            const li = document.createElement('li');
            li.className = 'item';
            li.innerText = values[i];
            ul.append(li);
        }

        return ul;
    }

    createItemsContainer(list: HTMLUListElement, headerTitle: string) {
        const container = document.createElement('div');
        container.classList.add('items-container');

        const header = document.createElement('header');
        header.innerText = headerTitle;

        const divFilter = document.createElement('div');
        divFilter.className = 'filter';
        const input = document.createElement('input');
        input.setAttribute("placeholder", "Search");
        input.setAttribute("type", "text");

        if (headerTitle === "Available") {
            input.addEventListener('input', (e) => this.handleFilter(e, this.liveUListSource));
        }
        else {
            input.addEventListener('input', (e) => this.handleFilter(e, this.liveUListTarget));
        }

        divFilter.append(input);
        container.append(header, divFilter, list);

        return container;
    }

    createButtons(content: string[], actions: ((e: Event) => void)[], amount: number) {
        const container = document.createElement('div');
        container.className = "buttons-container";

        for (let i = 0; i < amount; i++) {
            const button = document.createElement('button');
            button.innerText = content[i];
            button.addEventListener('click', actions[i]);
            container.append(button);
        }
        return container;
    }

    clearSelected(selected: HTMLLIElement[]) {
        selected.forEach(li => li.classList.remove('active'));

        while (selected.length) {
            selected.pop();
        }
    }

    //HANDLERS //this - ArrangeBox

    //CLICK ITEMS
    handleClickItem(event: Event, selected: 'selectedAvailable' | 'selectedTarget') {
        const item = event?.target as HTMLLIElement;
        if (item && item.classList.contains('item')) {
            if (item.classList.contains("active")) {
                this[selected] = this[selected].filter(li => li !== item);
                item.classList.remove('active');
            }
            else {
                this[selected].push(item);
                item.classList.add('active');
            }

        }
    }

    handleDBClickItem(event: Event, selected: HTMLLIElement[], to: Element) {
        const item = event?.target as HTMLLIElement;
        if (item && item.classList.contains('item')) {
            let li = event.target as HTMLLIElement;
            to.append(li);

            this.clearSelected(selected);
        }
    }

    //TRANSFER
    handleTransferTarget(e: Event) {
        this.liveUListTarget.append(...this.selectedAvailable);
        this.clearSelected(this.selectedAvailable);
    }
    handleTransferSource(e: Event) {
        this.liveUListSource.append(...this.selectedTarget);
        this.clearSelected(this.selectedTarget);
    }
    handleTransferAllTarget(e: Event) {
        this.liveUListTarget.append(...this.liveUListSource.children);
        this.clearSelected(this.selectedAvailable);
    }
    handleTransferAllSource(e: Event) {
        this.liveUListSource.append(...this.liveUListTarget.children);
        this.clearSelected(this.selectedTarget);
    }

    //FILTER
    handleFilter(e: Event, list: Element) {
        const input = e.target as HTMLInputElement;
        const value = input.value.trim().toLocaleLowerCase();

        for (let i = 0; i < list.children.length; i++) {
            const li = list.children[i] as HTMLLIElement;
            if (li.textContent?.toLocaleLowerCase()?.includes(value)) {
                li.style.display = "block";
            }
            else {
                li.style.display = "none";
            }
        }
    }

    //SORT
    handleAllDown(e: Event, list: Element, selected: HTMLLIElement[]) {
        list.append(...selected);
        // this.clearSelected(selected);
    }

    handleAllUp(e: Event, list: Element, selected: HTMLLIElement[]) {
        list.prepend(...selected);

        // this.clearSelected(selected);
    }

    handleDown(e: Event, list: Element, selected: HTMLLIElement[]) {
        if (selected.length === 0) return;

        // O(n^2)
        const sortSelected: HTMLLIElement[] = [];
        for (let i = 0; i < list.children.length; i++) {
            const li = selected.find(li => li === list.children[i]);
            if (li) {
                sortSelected.push(li);
            }
        }

        const last = sortSelected.length - 1;
        if (sortSelected[last].nextElementSibling) {
            for (let i = last; i >= 0; i--) {
                const li = sortSelected[i];
                li.nextElementSibling?.after(li);
            }
        }

        // this.clearSelected(selected);
    }

    handleUp(e: Event, list: Element, selected: HTMLLIElement[]) {
        if (selected.length === 0) return;

        // O(n^2)
        const sortSelected: HTMLLIElement[] = [];
        for (let i = 0; i < list.children.length; i++) {
            const li = selected.find(li => li === list.children[i]);
            if (li) {
                sortSelected.push(li);
            }
        }

        if (sortSelected[0]?.previousElementSibling) {
            for (let i = 0; i < sortSelected.length; i++) {
                const li = sortSelected[i];
                li.previousElementSibling?.before(li);
            }
        }

        // this.clearSelected(selected);
    }

    //CONTROLS

    setInitialValue() {
        this.clearSelected(this.selectedAvailable);
        this.clearSelected(this.selectedTarget);

        this.liveUListTarget.textContent = '';
        this.liveUListSource.textContent = '';

        this.liveUListSource.append(...this.initialListSource);
        this.liveUListTarget.append(...this.initialListTarget);
    }

    setAvailableValue(values: string[]) {
        this.clearSelected(this.selectedAvailable);
        this.liveUListSource.textContent = ''

        for (let i = 0; i < values.length; i++) {
            const li = document.createElement('li');
            li.innerHTML = values[i];
            this.liveUListSource.append(li);
        }
    }

    getSelected() {
        const values = [];
        for (let i = 0; i < this.liveUListTarget.children.length; i++) {
            const li = this.liveUListTarget.children[i];
            values.push(li.textContent);
        }
        alert(JSON.stringify(values));
        console.log(JSON.stringify(values));

        return values;
    }

    saveTarget() {
        const target = Array
            .from(this.liveUListTarget.children)
            .map(el => el.textContent);
        localStorage.setItem('target', JSON.stringify(target));
    }

    removeTarget() {
        localStorage.removeItem('target');
    }

    createNewInstance() {
        const newBox = new ArrangeBox();
    }
}

const arrangeBox = new ArrangeBox();